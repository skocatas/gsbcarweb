<?php

namespace ppe\GSBCarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ppe\GSBCarBundle\Entity\Electrique;

class AffichageController extends Controller
{
    /**
     * @Route("/voitureElectrique")
     * @Template("ppeGSBCarBundle:Afficher:afficherElectrique.html.twig")
     */
    public function voitureElectriqueAction()
    {       
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        $request = $this->get('request');
        $debut = $request->get('debut');
        $fin = $request->get('fin');
        $destination = $request->get('destination');
        
        if($username)
        {
            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $electrique = $em->getRepository('ppeGSBCarBundle:Electrique')->findAll();
            return array ('electrique' => $electrique, 'debut' => $debut, 'fin' => $fin, 'destination' => $destination);
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
        
         
    }
     /**
     * @Route("/voitureThermique")
     * @Template("ppeGSBCarBundle:Afficher:afficherThermique.html.twig")
     */
    public function voitureThermiqueAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        $request = $this->get('request');
        $debut = $request->get('debut');
        $fin = $request->get('fin');
        $destination = $request->get('destination');
        
        if($username)
        {
            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $thermique = $em->getRepository('ppeGSBCarBundle:Thermique')->findAll();
            return array ('thermique' => $thermique, 'debut' => $debut, 'fin' => $fin, 'destination' => $destination);         
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
    }
}
?>