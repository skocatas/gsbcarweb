<?php

namespace ppe\GSBCarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use ppe\GSBCarBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $session= $this->get('request')->getSession();
        $username= $session->get('username');
        if($username)
        {
            return $this->render('ppeGSBCarBundle:Formulaire:choixVoiture.html.twig',  array('message' => 'Vous êtes déjà connecté'));
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig');
        }   
    }
    
    /**
     * @Route("/connexion")
     */
    public function connexionAction()
    {
        $session = $this->get('request')->getSession();
        $request = $this->get('request');
        /*$session = new Session();
        $session->start();*/
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        
        $repository = $this->getDoctrine()->getRepository('ppeGSBCarBundle:User');

        // query for a single product matching the given name and price
        $user = $repository->findOneBy(
            array('identifiant' => $username, 'password' => $password)
        );
        dump($user);
        
        $userS = $session->get('username');
        if($userS)
        {        
            $id = $session->get('id');
            dump($id);
            return $this->render('ppeGSBCarBundle:Formulaire:choixVoiture.html.twig', array ('message' => 'Vous êtes déjà connecté'));
        }
        else
        {
        if($user)
        {
            $idU = $user->getId();
            $session->set('username', $username);
            $session->set('password', $password);
            $session->set('id', $idU);
            return $this->render('ppeGSBCarBundle:Formulaire:choixVoiture.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array(
               'message'=>'Erreur de login ou de mot de passe '));            
        }       
        }         
    }
    
    /**
     * @Route("/deconnexion")
     */
    public function deconnexionAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        if($username)
        {
        $this->get('session')->clear();
        return $this->render('ppeGSBCarBundle:Default:connexion.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
    }
    
 }
