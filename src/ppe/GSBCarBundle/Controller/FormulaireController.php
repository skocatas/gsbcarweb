<?php

namespace ppe\GSBCarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ppe\GSBCarBundle\Entity\Reservation;



class FormulaireController extends Controller
{
    /**
     * @Route("/choix")
     */
    public function choixAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        if($username)
        {
            return $this->render('ppeGSBCarBundle:Formulaire:choixVoiture.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
    }

    /**
     * @Route("/electrique")
     */
    public function electriqueAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');      
        
        if($username)
        {
            
            return $this->render('ppeGSBCarBundle:Formulaire:formulaireElectrique.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
    }
    
     /**
     * @Route("/thermique")
     */
    public function thermiqueAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        if($username)
        {
            return $this->render('ppeGSBCarBundle:Formulaire:formulaireThermique.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
       
    }
    
    /**
     * @Route("/depart")
     */
    public function departAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        if($username)
        {
             return $this->render('ppeGSBCarBundle:Formulaire:formulaireDepart.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
    }
    /**
     * @Route("/retour")
     */
    public function retourAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        if($username)
        {
            return $this->render('ppeGSBCarBundle:Formulaire:formulaireRetour.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
    }
    /**
     * @Route("/reserverElectrique")
     */
    public function reserverElectriqueAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');      
        
        if($username)
        {
            $request = $this->get('request');
            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();

            $id = $session->get('id');
            $u = $em->find('ppeGSBCarBundle:User', $id);
            $ve = $em->find('ppeGSBCarBundle:Electrique', $request->query->get('idVoiture'));
            $debut = $request->query->get('debut');
            $fin = $request->query->get('fin');
            $destination = $request->query->get('destination');
            
            dump($ve);

            // Création de l'entité
            $rElectrique = new Reservation();
            $rElectrique->setDateDebut(\Datetime::createFromFormat('d-m-Y', date( "d-m-Y", strtotime($debut) )));
            $rElectrique->setDateFin(\Datetime::createFromFormat('d-m-Y', date( "d-m-Y", strtotime($fin) )));
            $rElectrique->setDestination($destination);
            $rElectrique->setUser($u);
            $rElectrique->setVElectrique($ve);


            //gestion de $client par l’ORM
            $em->persist($rElectrique);

            //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
            $em->flush();

            //redirection ou affichage d’une vue
            return $this->render('ppeGSBCarBundle:Default:index.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
        
    }
    
    /**
     * @Route("/reserverThermique")
     */
    public function reserverThermiqueAction()
    {
        $session= $this->get('request')->getSession();
        $username = $session->get('username');
        
        if($username)
        {
            $request = $this->get('request');
            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();

            $id = $session->get('id');
            $u = $em->find('ppeGSBCarBundle:User', $id);
            $vt = $em->find('ppeGSBCarBundle:Thermique', $request->query->get('idVoiture'));
            $debut = $request->query->get('debut');
            $fin = $request->query->get('fin');
            $destination = $request->query->get('destination');
            
            dump($vt);

            // Création de l'entité
            $vThermique = new Reservation();
            $vThermique->setDateDebut(\Datetime::createFromFormat('d-m-Y', date( "d-m-Y", strtotime( $debut ) )));
            $vThermique->setDateFin(\Datetime::createFromFormat('d-m-Y', date( "d-m-Y", strtotime( $fin ) )));
            $vThermique->setDestination($destination);
            $vThermique->setUser($u);
            $vThermique->setVThermique($vt);


            //gestion de $client par l’ORM
            $em->persist($vThermique);

            //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
            $em->flush();

            //redirection ou affichage d’une vue
            return $this->render('ppeGSBCarBundle:Default:index.html.twig');
        }
        else
        {
            return $this->render('ppeGSBCarBundle:Default:connexion.html.twig', array ('message' => 'Veuillez vous connecter !'));
        }
        
        
    }
}
