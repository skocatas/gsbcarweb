<?php
/** 
 * Fonctions pour l'application GSB 
 * @package default
 * @author Semih
 */

 /**
 * Teste si un quelconque visiteur est connecté
 * @return vrai ou faux 
 */

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

function estConnecte($session){
  //return isset($_SESSION['idVisiteur']);
//    $request = Request::createFromGlobals();
//    $session = $request->getSession();
      return $session->getFlashBag()->has('id');
}
/**
 * Enregistre dans une variable session les infos d'un visiteur
 
 * @param $id 
 * @param $nom
 * @param $prenom
 */
function connecter($session, $id,$nom,$prenom)
{
//	$_SESSION['idVisiteur']= $id; 
//	$_SESSION['nom']= $nom;
////	$_SESSION['prenom']= $prenom;
//     $request = Request::createFromGlobals();
//    $session = $request->getSession();
        $session->getFlashBag()->add('id',$id);
        $session->getFlashBag()->add('nom',$nom);
        $session->getFlashBag()->add('prenom',$prenom);     
}
/**
 * Détruit la session active
 */
function deconnecter()
{
    session_destroy();
}
/**
 * Ajoute le libellé d'une erreur au tableau des erreurs 
 
 * @param $msg : le libellé de l'erreur 
 */
function ajouterErreur($session, $msg)
{
    $session->getFlashBag()->add('erreurs',$msg);   
}
function existeErreurs($session)
{  
    return $session->getFlashBag()->has('erreurs');
}
function getLesErreurs($session)
{   
    return  $session->getFlashBag()->get('erreurs');
}

?>