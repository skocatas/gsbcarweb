<?php

namespace ppe\GSBCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="IDX_42C84955F8589C75", columns={"v_thermique_id"}), @ORM\Index(name="IDX_42C849559E9D18D5", columns={"v_electrique_id"}), @ORM\Index(name="IDX_42C84955A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Reservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetime", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetime", nullable=false)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255, nullable=false)
     */
    private $destination;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Electrique
     *
     * @ORM\ManyToOne(targetEntity="Electrique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="v_electrique_id", referencedColumnName="id")
     * })
     */
    private $vElectrique;

    /**
     * @var \Thermique
     *
     * @ORM\ManyToOne(targetEntity="Thermique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="v_thermique_id", referencedColumnName="id")
     * })
     */
    private $vThermique;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Reservation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Reservation
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Reservation
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set user
     *
     * @param \ppe\GSBCarBundle\Entity\User $user
     * @return Reservation
     */
    public function setUser(\ppe\GSBCarBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ppe\GSBCarBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set vElectrique
     *
     * @param \ppe\GSBCarBundle\Entity\Electrique $vElectrique
     * @return Reservation
     */
    public function setVElectrique(\ppe\GSBCarBundle\Entity\Electrique $vElectrique = null)
    {
        $this->vElectrique = $vElectrique;

        return $this;
    }

    /**
     * Get vElectrique
     *
     * @return \ppe\GSBCarBundle\Entity\Electrique 
     */
    public function getVElectrique()
    {
        return $this->vElectrique;
    }

    /**
     * Set vThermique
     *
     * @param \ppe\GSBCarBundle\Entity\Thermique $vThermique
     * @return Reservation
     */
    public function setVThermique(\ppe\GSBCarBundle\Entity\Thermique $vThermique = null)
    {
        $this->vThermique = $vThermique;

        return $this;
    }

    /**
     * Get vThermique
     *
     * @return \ppe\GSBCarBundle\Entity\Thermique 
     */
    public function getVThermique()
    {
        return $this->vThermique;
    }
}
