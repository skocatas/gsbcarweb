<?php

namespace ppe\GSBCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReservationDepart
 *
 * @ORM\Table(name="reservation__depart", indexes={@ORM\Index(name="IDX_8598E70EBA31B7B", columns={"la_reservation_id"})})
 * @ORM\Entity
 */
class ReservationDepart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat", type="string", length=50, nullable=false)
     */
    private $etat;

    /**
     * @var integer
     *
     * @ORM\Column(name="Kilometrage", type="integer", nullable=false)
     */
    private $kilometrage;

    /**
     * @var \Reservation
     *
     * @ORM\ManyToOne(targetEntity="Reservation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="la_reservation_id", referencedColumnName="id")
     * })
     */
    private $laReservation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return ReservationDepart
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return ReservationDepart
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set laReservation
     *
     * @param \ppe\GSBCarBundle\Entity\Reservation $laReservation
     * @return ReservationDepart
     */
    public function setLaReservation(\ppe\GSBCarBundle\Entity\Reservation $laReservation = null)
    {
        $this->laReservation = $laReservation;

        return $this;
    }

    /**
     * Get laReservation
     *
     * @return \ppe\GSBCarBundle\Entity\Reservation 
     */
    public function getLaReservation()
    {
        return $this->laReservation;
    }
}
