<?php

namespace ppe\GSBCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReservationRetour
 *
 * @ORM\Table(name="reservation__retour", indexes={@ORM\Index(name="IDX_73C98F27BA31B7B", columns={"la_reservation_id"})})
 * @ORM\Entity
 */
class ReservationRetour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Kilometrage", type="integer", nullable=false)
     */
    private $kilometrage;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat", type="string", length=255, nullable=false)
     */
    private $etat;

    /**
     * @var \Reservation
     *
     * @ORM\ManyToOne(targetEntity="Reservation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="la_reservation_id", referencedColumnName="id")
     * })
     */
    private $laReservation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return ReservationRetour
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return ReservationRetour
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set laReservation
     *
     * @param \ppe\GSBCarBundle\Entity\Reservation $laReservation
     * @return ReservationRetour
     */
    public function setLaReservation(\ppe\GSBCarBundle\Entity\Reservation $laReservation = null)
    {
        $this->laReservation = $laReservation;

        return $this;
    }

    /**
     * Get laReservation
     *
     * @return \ppe\GSBCarBundle\Entity\Reservation 
     */
    public function getLaReservation()
    {
        return $this->laReservation;
    }
}
