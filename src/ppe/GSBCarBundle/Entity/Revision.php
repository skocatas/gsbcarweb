<?php

namespace ppe\GSBCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Revision
 *
 * @ORM\Table(name="revision", indexes={@ORM\Index(name="IDX_6D6315CCF8589C75", columns={"v_thermique_id"}), @ORM\Index(name="IDX_6D6315CC9E9D18D5", columns={"v_electrique_id"})})
 * @ORM\Entity
 */
class Revision
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date_debut", type="string", length=50, nullable=false)
     */
    private $dateDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="date_fin", type="string", length=50, nullable=false)
     */
    private $dateFin;

    /**
     * @var \Electrique
     *
     * @ORM\ManyToOne(targetEntity="Electrique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="v_electrique_id", referencedColumnName="id")
     * })
     */
    private $vElectrique;

    /**
     * @var \Thermique
     *
     * @ORM\ManyToOne(targetEntity="Thermique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="v_thermique_id", referencedColumnName="id")
     * })
     */
    private $vThermique;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param string $dateDebut
     * @return Revision
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return string 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param string $dateFin
     * @return Revision
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return string 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set vElectrique
     *
     * @param \ppe\GSBCarBundle\Entity\Electrique $vElectrique
     * @return Revision
     */
    public function setVElectrique(\ppe\GSBCarBundle\Entity\Electrique $vElectrique = null)
    {
        $this->vElectrique = $vElectrique;

        return $this;
    }

    /**
     * Get vElectrique
     *
     * @return \ppe\GSBCarBundle\Entity\Electrique 
     */
    public function getVElectrique()
    {
        return $this->vElectrique;
    }

    /**
     * Set vThermique
     *
     * @param \ppe\GSBCarBundle\Entity\Thermique $vThermique
     * @return Revision
     */
    public function setVThermique(\ppe\GSBCarBundle\Entity\Thermique $vThermique = null)
    {
        $this->vThermique = $vThermique;

        return $this;
    }

    /**
     * Get vThermique
     *
     * @return \ppe\GSBCarBundle\Entity\Thermique 
     */
    public function getVThermique()
    {
        return $this->vThermique;
    }
}
